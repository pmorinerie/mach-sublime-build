This repository contains a file for integrating Mozilla’s [Mach](https://developer.mozilla.org/en-US/docs/Mozilla/Developer_guide/mach) build system into [Sublime Text 3](https://www.sublimetext.com/).

## Installation

1. Download the [Mach.sublime-build](https://gitlab.com/pmorinerie/mach-sublime-build/raw/master/Mach.sublime-build) file,
2. Open Sublime Text Packages directory (using the `Preferences ▸ Browse Package…` menu item),
3. Copy `Mach.sublime-build` inside the Packages `User` sub-directory.

## Usage

Sublime Text will automatically set the build system to `Mach` when working in a folder containing a root-level `mach` file.

_You can also configure it manually, by setting the `Tools ▸ Build System ▸ Mach` setting._

Then:

- Use `Tools ▸ Build with…` (or `Shift+Ctrl+B`) to select the build commands:
  - `Mach build` build and run the browser (default).
  - `Mach - Build faster`: run `mach build faster`, which only copies Javascript files without recompiling native files.
  - `Mach - Test file`: run tests for the currently opened test file.
- Use `Tools ▸ Build` (or `Ctrl+B`) to run the last used build action again.

For each build action the output logs will be displayed in a Sublime Text panel.
